<?

require_once('config.php');
require_once('model.php');
require_once('classes/coin.php');

$params = get_params();

$coins = get_coins($params);

$menu = get_menu();

if (isset($params['period_id']) && $params['period_id'] != '') {
	$period = get_period((int)$params['period_id']);
} else {
	$period = new stdClass();
	$period->name = "Россия";
	$period->id = 0;
}

// Загружаем справочники
$nominals = get_nominals();
$mints = get_mints();
$qualities = get_qualities();
$materials = get_materials();
$series = get_series();
$types = array(
	1 => "Регулярный чекан",
	2 => "Юбилейный чекан"
);
$years = get_years($period->id);

if (isset($params['coin_id'])) {
	$coin = Coin::get($params['coin_id']);
	$period = get_period($coin->period_id);
	include('tmpl/coin.php');
} else {
	$coins = get_coins($params);
	include('tmpl/coins.php');
}

?>