<?
chdir('..');
require_once('config.php');
require_once('model.php');
require_once('classes/coin.php');

$params = get_params();

$menu = get_menu();
$stages = get_menu();

if (isset($params['period_id'])) {
	$period = get_period((int)$params['period_id']);
} else {
	$period = new stdClass();
	$period->name = "Россия";
	$period->id = 0;
}

if (isset($params['task']))
	$task = $params['task'];

// Загружаем справочники
$nominals = get_nominals();
$mints = get_mints();
$qualities = get_qualities();
$materials = get_materials();
$series = get_series();
$types = array(
	1 => "Регулярный чекан",
	2 => "Юбилейный чекан"
);

if ($_POST) {
	$input = clean_array($_POST);

	if (isset($input['id'])) {
		$coin = Coin::get((int)$input['id']);
		$period = get_period($coin->period_id);

		foreach ($input as $key => $value) {
			$coin->$key = $value;
		}

		if ($coin->update()) {
			header('Location: /admin/coins.php?period_id='.$period->id);
		} else {
			include('tmpl/admin/coin_form.php');
		}
	} else {
		$coin = new Coin($input);
		if ($coin->save()) {
			header('Location: /admin/coins.php?period_id='.$period->id);
		} else {
			include('tmpl/admin/coin_form.php');
		}
	}
} else {
	if (isset($params['task'])) {
		switch ($params['task']) {
			case 'new':
				$coin = new Coin();
				include('tmpl/admin/coin_form.php');
				break;

			case 'delete':
				if (isset($params['coin_id'])) {
					$coin = Coin::get($params['coin_id']);
					$coin->delete();
					header('Location: /admin/coins.php?period_id='.$coin->period_id);
				}
				break;

			case 'edit':
				if (isset($params['coin_id'])) {
					$coin = Coin::get($params['coin_id']);
					$period = get_period($coin->period_id);
					include('tmpl/admin/coin_form.php');
				}
				break;

			default:
				$coins = get_coins($params);
				include('tmpl/admin/coins.php');
				break;
		}
	} elseif (isset($params['coin_id'])) {
		$coin = Coin::get($params['coin_id']);
		$period = get_period($coin->period_id);
		include('tmpl/admin/coin.php');
	} else {
		$coins = get_coins($params);
		include('tmpl/admin/coins.php');
	}
}

?>
