-- phpMyAdmin SQL Dump
-- version 4.2.11
-- http://www.phpmyadmin.net
--
-- Хост: localhost
-- Время создания: Мар 07 2015 г., 00:21
-- Версия сервера: 5.6.21
-- Версия PHP: 5.5.19

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- База данных: `coins_catalog`
--

-- --------------------------------------------------------

--
-- Структура таблицы `coins`
--

CREATE TABLE IF NOT EXISTS `coins` (
`id` int(11) NOT NULL,
  `period_id` int(11) NOT NULL,
  `type_id` int(11) NOT NULL,
  `serios_id` int(11) NOT NULL,
  `nominal_id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `year` int(5) NOT NULL,
  `obverse_desc` text NOT NULL,
  `reverse_desc` text NOT NULL,
  `mint_id` int(11) NOT NULL,
  `mintage` varchar(255) NOT NULL,
  `quality_id` int(11) NOT NULL,
  `material_id` int(11) NOT NULL,
  `release` date NOT NULL,
  `sample` varchar(10) NOT NULL,
  `diameter` varchar(10) NOT NULL,
  `weight` varchar(10) NOT NULL,
  `thickness` varchar(10) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `coins`
--

INSERT INTO `coins` (`id`, `period_id`, `type_id`, `serios_id`, `nominal_id`, `name`, `year`, `obverse_desc`, `reverse_desc`, `mint_id`, `mintage`, `quality_id`, `material_id`, `release`, `sample`, `diameter`, `weight`, `thickness`) VALUES
(9, 17, 1, 0, 5, '10 копеек 1991 года', 1991, 'Рельефное изображение Спасской башни Московского Кремля, а так же свод купола здания Верховного Совета СССР. По окружности монеты слева направо отчеканена надпись «Государственный Банк СССР».', 'В центре - номинал монеты «10», немного ниже надпись «Копеек», под номиналом две сужающиеся к краям линии, разделенные посередине товарным знаком Московского монетного двора, под линиями год выпуска монеты «1991». С левой стороны монеты изображен колос, а справа ветвь дубовых листьев с желудем.', 1, '', 1, 5, '0000-00-00', '', '17.6 мм', '2.3 г', '1.25 мм'),
(10, 17, 1, 0, 8, '50 копеек 1991 года', 1991, 'Рельефное изображение Спасской башни Московского Кремля, а так же свод купола здания Верховного Совета СССР. По окружности монеты слева направо отчеканена надпись «Государственный Банк СССР».', 'В центре - номинал монеты «50», немного ниже надпись «Копеек», под номиналом две сужающиеся к краям линии, разделенные посередине товарным знаком Московского монетного двора, под линиями год выпуска монеты «1991». С левой стороны монеты изображен колос, а справа ветвь дубовых листьев с желудем.', 2, '', 1, 6, '0000-00-00', '', '18.0 мм', '2.3 г', '1.25 мм'),
(11, 17, 1, 0, 9, '1 рубль 1991 года', 1991, 'Рельефное изображение Спасской башни Московского Кремля, а так же свод купола здания Верховного Совета СССР. По окружности монеты слева направо отчеканена надпись «Государственный Банк СССР».', 'В центре - номинал монеты «1», немного ниже надпись «Копеек», под номиналом две сужающиеся к краям линии, разделенные посередине товарным знаком Московского монетного двора, под линиями год выпуска монеты «1991». С левой стороны монеты изображен колос, а справа ветвь дубовых листьев с желудем.', 1, '', 1, 6, '0000-00-00', '', '21.0 мм', '3.75 г', '1.45 мм'),
(12, 17, 1, 0, 10, '10 рублей 1991 года', 1991, 'Рельефное изображение Спасской башни Московского Кремля, а так же свод купола здания Верховного Совета СССР. По окружности монеты слева направо отчеканена надпись «Государственный Банк СССР».', 'В центре - номинал монеты «10», немного ниже надпись «Рублей», под номиналом две сужающиеся к краям линии, разделенные посередине товарным знаком Московского монетного двора, под линиями год выпуска монеты «1991». С левой стороны монеты изображен колос, а справа ветвь дубовых листьев с желудем.', 1, '', 1, 7, '0000-00-00', '', '25.0 мм', '6.25 г', '1.95 мм'),
(13, 17, 1, 0, 10, '10 рублей 1992 года', 1992, 'Рельефное изображение Спасской башни Московского Кремля, а так же свод купола здания Верховного Совета СССР. По окружности монеты слева направо отчеканена надпись «Государственный Банк СССР».', 'В центре - номинал монеты «10», немного ниже надпись «Рублей», под номиналом две сужающиеся к краям линии, разделенные посередине товарным знаком Московского монетного двора, под линиями год выпуска монеты «1992». С левой стороны монеты изображен колос, а справа ветвь дубовых листьев с желудем.', 1, '', 1, 7, '0000-00-00', '', '25.0 мм', '6.25 г', '1.95 мм');

-- --------------------------------------------------------

--
-- Структура таблицы `materials`
--

CREATE TABLE IF NOT EXISTS `materials` (
`id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `materials`
--

INSERT INTO `materials` (`id`, `name`) VALUES
(1, 'Алюминий'),
(2, 'Бронза'),
(3, 'Золото'),
(4, 'Серебро'),
(5, 'Сталь, плакированная латунью'),
(6, 'Медно-никелевый сплав '),
(7, 'Биметалл');

-- --------------------------------------------------------

--
-- Структура таблицы `mints`
--

CREATE TABLE IF NOT EXISTS `mints` (
`id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `mints`
--

INSERT INTO `mints` (`id`, `name`) VALUES
(1, 'Московский монетный двор'),
(2, 'Санкт-Петербургский монетный двор'),
(3, 'Екатеринбургский монетный двор'),
(4, 'Сузунский монетный двор'),
(5, 'Пермский монетный двор'),
(6, 'Сестрорецкий монетный двор'),
(7, 'Банковский монетный двор'),
(8, 'Ижорский монетный двор'),
(9, 'Кадашевский монетный двор'),
(10, 'Набережный медный монетный двор'),
(11, 'Набережный серебряный монетный двор'),
(12, 'Ленинградский монетный двор'),
(13, 'Петроградский монетный двор');

-- --------------------------------------------------------

--
-- Структура таблицы `nominals`
--

CREATE TABLE IF NOT EXISTS `nominals` (
`id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `nominals`
--

INSERT INTO `nominals` (`id`, `name`) VALUES
(1, '1 копейка'),
(2, '2 копейки '),
(3, '3 копейки'),
(4, '5 копеек'),
(5, '10 копеек'),
(6, '15 копеек'),
(7, '20 копеек'),
(8, '50 копеек'),
(9, '1 рубль'),
(10, '10 рублей');

-- --------------------------------------------------------

--
-- Структура таблицы `periods`
--

CREATE TABLE IF NOT EXISTS `periods` (
`id` int(11) NOT NULL,
  `stage_id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=29 DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `periods`
--

INSERT INTO `periods` (`id`, `stage_id`, `name`) VALUES
(1, 1, 'Петр I (1699-1725)'),
(2, 1, 'Екатерина I (1725-1727)'),
(3, 1, 'Петр II (1727-1729)'),
(4, 1, 'Анна Иоанновна (1730-1740)'),
(5, 1, 'Иоанн Антонович (1740-1741)'),
(6, 1, 'Елизавета (1741-1762)'),
(7, 1, 'Петр III (1762-1762)'),
(8, 1, 'Екатерина II (1762-1796)'),
(9, 1, 'Павел I (1796-1801)'),
(10, 1, 'Александр I (1801-1825)'),
(11, 1, 'Николай I (1826-1855)'),
(12, 1, 'Александр II (1854-1881)'),
(13, 1, 'Александр III (1881-1894)'),
(14, 1, 'Николай II (1894-1918)'),
(15, 2, 'СССР 1961-1991 гг.'),
(16, 2, 'РСФСР-СССР 1921-1957 гг.'),
(17, 2, 'Гос.банк СССР 1991-1992 гг.'),
(18, 2, 'Юбилейные СССР 1965-1991 гг.'),
(19, 2, 'Пробные монеты 1921-1991 гг.'),
(20, 2, 'Юбилейные из драг. металлов'),
(21, 2, 'Инвестиционные монеты'),
(22, 3, 'Россия с 1997 года'),
(23, 3, 'Юбилейные с 1999 года'),
(24, 3, 'Россия 1992-1993 гг.'),
(25, 3, 'Юбилейные 1992-1996 гг.'),
(26, 3, 'Коллекционные 1, 2, 3 рубля '),
(27, 3, 'Коллекционные монеты '),
(28, 3, 'Инвестиционные монеты');

-- --------------------------------------------------------

--
-- Структура таблицы `qualities`
--

CREATE TABLE IF NOT EXISTS `qualities` (
`id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `qualities`
--

INSERT INTO `qualities` (`id`, `name`) VALUES
(1, 'Анциркулейтед'),
(2, 'Бриллиант-анциркулейтед'),
(3, 'Пруф'),
(4, 'Пруф-лайк'),
(5, 'Реверс фростед');

-- --------------------------------------------------------

--
-- Структура таблицы `series`
--

CREATE TABLE IF NOT EXISTS `series` (
`id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `series`
--

INSERT INTO `series` (`id`, `name`) VALUES
(1, 'Эпоха просвещения. XVIII век'),
(2, 'Вклад России в сокровищницу мировой культуры'),
(3, '1000-летие России'),
(4, '300-летие основания Санкт-Петербурга');

-- --------------------------------------------------------

--
-- Структура таблицы `stages`
--

CREATE TABLE IF NOT EXISTS `stages` (
`id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `stages`
--

INSERT INTO `stages` (`id`, `name`) VALUES
(1, 'Российская Империя'),
(2, 'СССР'),
(3, 'Российская Федерация');

--
-- Индексы сохранённых таблиц
--

--
-- Индексы таблицы `coins`
--
ALTER TABLE `coins`
 ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `materials`
--
ALTER TABLE `materials`
 ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `mints`
--
ALTER TABLE `mints`
 ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `nominals`
--
ALTER TABLE `nominals`
 ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `periods`
--
ALTER TABLE `periods`
 ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `qualities`
--
ALTER TABLE `qualities`
 ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `series`
--
ALTER TABLE `series`
 ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `stages`
--
ALTER TABLE `stages`
 ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT для сохранённых таблиц
--

--
-- AUTO_INCREMENT для таблицы `coins`
--
ALTER TABLE `coins`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=14;
--
-- AUTO_INCREMENT для таблицы `materials`
--
ALTER TABLE `materials`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT для таблицы `mints`
--
ALTER TABLE `mints`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=14;
--
-- AUTO_INCREMENT для таблицы `nominals`
--
ALTER TABLE `nominals`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=11;
--
-- AUTO_INCREMENT для таблицы `periods`
--
ALTER TABLE `periods`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=29;
--
-- AUTO_INCREMENT для таблицы `qualities`
--
ALTER TABLE `qualities`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT для таблицы `series`
--
ALTER TABLE `series`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT для таблицы `stages`
--
ALTER TABLE `stages`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=4;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
