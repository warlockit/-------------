<?

class Coin {
	public $id;
	public $name;
	public $year;
	public $period_id;
	public $type_id;
	public $serios_id;
	public $nominal_id;
	public $obverse_desc;
	public $reverse_desc;
	public $mint_id;
	public $quality_id;
	public $material_id;
	public $mintage;
	public $sample;
	public $diameter;
	public $weight;
	public $thickness;
	public $release;

	public function __construct($params = array()) {
		if (isset($params['id']))
			$this->id = $params['id'];
		if (isset($params['name']))
			$this->name = $params['name'];
		if (isset($params['year']))
			$this->year = $params['year'];
		if (isset($params['period_id']))
			$this->period_id = $params['period_id'];
		if (isset($params['type_id']))
			$this->type_id = $params['type_id'];
		if (isset($params['serios_id']))
			$this->serios_id = $params['serios_id'];
		if (isset($params['nominal_id']))
			$this->nominal_id = $params['nominal_id'];
		if (isset($params['obverse_desc']))
			$this->obverse_desc = $params['obverse_desc'];
		if (isset($params['reverse_desc']))
			$this->reverse_desc = $params['reverse_desc'];
		if (isset($params['mint_id']))
			$this->mint_id = $params['mint_id'];
		if (isset($params['quality_id']))
			$this->quality_id = $params['quality_id'];
		if (isset($params['material_id']))
			$this->material_id = $params['material_id'];
		if (isset($params['mintage']))
			$this->mintage = $params['mintage'];
		if (isset($params['sample']))
			$this->sample = $params['sample'];
		if (isset($params['diameter']))
			$this->diameter = $params['diameter'];
		if (isset($params['weight']))
			$this->weight = $params['weight'];
		if (isset($params['thickness']))
			$this->thickness = $params['thickness'];
		if (isset($params['release']))
			$this->release = $params['release'];
	}

	public function get($id) {

		if (isset($id)) {
			$result = mysql_query("SELECT * FROM `coins` WHERE `id` = ".$id." LIMIT 1");
		
			while ($row = mysql_fetch_array($result)) {
				return new Coin($row);
			}
		}
		return false;
	}


	public function save() {
		$query = mysql_query("INSERT INTO `coins` VALUES (NULL,'$this->period_id', '$this->type_id', '$this->serios_id',
			'$this->nominal_id', '$this->name', '$this->year', '$this->obverse_desc', '$this->reverse_desc', '$this->mint_id', 
			'$this->mintage', '$this->quality_id', '$this->material_id', '$this->release', '$this->sample', '$this->diameter', 
			'$this->weight', '$this->thickness')");

		if ($query) {
			return true;
		} else {
			echo mysql_error();
		}
	}

	public function update() {
		$query = mysql_query("UPDATE `coins` SET `period_id`='$this->period_id', 
			`type_id` = '$this->type_id', 
			`serios_id` = '$this->serios_id',
			`nominal_id` = '$this->nominal_id', 
			`name` = '$this->name', 
			`year` = '$this->year', 
			`obverse_desc` = '$this->obverse_desc', 
			`reverse_desc` = '$this->reverse_desc', 
			`mint_id` = '$this->mint_id', 
			`mintage` = '$this->mintage', 
			`quality_id` = '$this->quality_id', 
			`material_id` = '$this->material_id', 
			`release` = '$this->release', 
			`sample` = '$this->sample', 
			`diameter` = '$this->diameter', 
			`weight` = '$this->weight', 
			`thickness` = '$this->thickness' WHERE `id` = $this->id");

		if ($query) {
			return true;
		} else {
			echo mysql_error();
		}
	}

	public function delete() {
		if (isset($this->id)) {
			$result = mysql_query("DELETE FROM `coins` WHERE `id` = $this->id");
			return true;
		}
		return false;
	}
}

?>