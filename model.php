<?
$message = '';

if ($db = @mysql_connect(HOST_CONNECT, LOGIN_CONNECT, PASSW_CONNECT)) {
	mysql_select_db(DB_CONNECT);
	mysql_set_charset("utf8");
} else {
	$message = "Не удалось подключится к базе данных";
}

function get_params() {
	if (count($_GET)) return clean_array($_GET);
	else return false;
}

function clean_array($array) {
	if (count($array)) {
		$array = array_map("strip_tags", $array);
		$array = array_map("trim", $array);
		$array = array_map("mysql_real_escape_string", $array);
		return $array;
	} else {
		return false;
	}
}

function get_coins($params = array()) {
	$filter = '';

	if (isset($params['period_id']) && $params['period_id'] > 0) {
		$filter .= " AND `period_id` = ".$params["period_id"];
	}

	if (isset($params['nominal_id']) && $params['nominal_id'] > 0) {
		$filter .= " AND `nominal_id` = ".$params["nominal_id"];
	}

	if (isset($params['mint_id']) && $params['mint_id'] > 0) {
		$filter .= " AND `mint_id` = ".$params["mint_id"];
	}

	if (isset($params['year']) && $params['year'] > 0) {
		$filter .= " AND `year` = ".$params["year"];
	}

	$result = mysql_query("SELECT * FROM `coins` WHERE 1".$filter);

	$number = mysql_num_rows($result);

	if ($number != 0) {
		while ($row = mysql_fetch_object($result)) {
			$coins[] = $row;
		}
		return $coins;
	}
	return false;
}

function get_menu() {
	$result = mysql_query("SELECT * FROM `stages`");
	$number = mysql_num_rows($result);

	if ($number != 0) {
		while ($stage = mysql_fetch_object($result)) {
			$stage->periods = get_periods($stage->id);
			$stages[] = $stage;
		}
		return $stages;
	}
	return false;
}

function get_periods($stage_id) {
	if (isset($stage_id)) {
		$stage_id = (int)$stage_id;
		$result = mysql_query("SELECT * FROM `periods` WHERE `stage_id` = $stage_id");
		$number = mysql_num_rows($result);

		if ($number != 0) {
			while ($period = mysql_fetch_object($result)) {
				$periods[] = $period;
			}
			return $periods;
		}
	}

	return false;
}

function get_years($period_id) {
	if (isset($period_id) && $period_id != '0') {
		$result = mysql_query("SELECT DISTINCT `year` FROM `coins` WHERE `period_id` = $period_id");
	} else {
		$result = mysql_query("SELECT DISTINCT `year` FROM `coins`");
	}

	while ($year = mysql_fetch_row($result)) {
		$years[] = $year[0];
	}
	if (isset($years))
		return $years;
	
	return false;
}

function get_period($period_id) {
	if (isset($period_id)) {
		$period_id = (int)$period_id;
		$result = mysql_query("SELECT * FROM `periods` WHERE `id` = $period_id");
		
		while ($period = mysql_fetch_object($result)) {
			return $period;
		}
	}

	return false;
}

function get_nominals() {
	$result = mysql_query("SELECT * FROM `nominals`");

	$number = mysql_num_rows($result);

	if ($number != 0) {
		while ($row = mysql_fetch_array($result)) {
			$nominals[] = $row;
		}

		foreach ($nominals as $nominal) {
			$nominals_temp[$nominal['id']] = $nominal['name'];
		}

		return $nominals_temp;
	}
	return false;
}

function get_mints() {
	$result = mysql_query("SELECT * FROM `mints`");

	$number = mysql_num_rows($result);

	if ($number != 0) {
		while ($row = mysql_fetch_array($result)) {
			$mints[] = $row;
		}

		foreach ($mints as $mint) {
			$mints_temp[$mint['id']] = $mint['name'];
		}
		
		return $mints_temp;
	}
	return false;
}

function get_qualities() {
	$result = mysql_query("SELECT * FROM `qualities`");

	$number = mysql_num_rows($result);

	if ($number != 0) {
		while ($row = mysql_fetch_array($result)) {
			$qualities[] = $row;
		}

		foreach ($qualities as $quality) {
			$qualities_temp[$quality['id']] = $quality['name'];
		}
		
		return $qualities_temp;
	}
	return false;
}

function get_materials() {
	$result = mysql_query("SELECT * FROM `materials`");

	$number = mysql_num_rows($result);

	if ($number != 0) {
		while ($row = mysql_fetch_array($result)) {
			$materials[] = $row;
		}

		foreach ($materials as $material) {
			$materials_temp[$material['id']] = $material['name'];
		}
		
		return $materials_temp;
	}
	return false;
}

function get_series() {
	$result = mysql_query("SELECT * FROM `series`");

	$number = mysql_num_rows($result);

	if ($number != 0) {
		while ($row = mysql_fetch_array($result)) {
			$series[] = $row;
		}

		foreach ($series as $serios) {
			$series_temp[$serios['id']] = $serios['name'];
		}
		
		return $series_temp;
	}
	return false;
}

?>