<? include('tmpl/admin/header.php'); ?>
<ol class="breadcrumb">
	  <li><a href="/admin/coins.php">Монеты</a></li>
	  <? if ($period->id != 0) { ?>
	  <li><a href="/admin/coins.php?period_id=<?= $period->id ?>"><?= $period->name ?></a></li>
	  <? } ?>
	  <li class="active"><?= $coin->name ?></li>
</ol>
<?= $message ?>
<h1><?= $coin->name ?></h1>
<div class="col-md-8">
	<h3>Описание монеты</h3>
	<table class="table table-striped table-bordered">
		<tr>
			<td>
				Период
			</td>
			<td>
				<?= $period->name ?>
			</td>
		</tr>
		<tr>
			<td>
				Тип чеканки
			</td>
			<td>
				<?= ($coin->type_id == 1 ? 'Регулярный чекан' : 'Юбилейны чекан') ?>
			</td>
		</tr>
		<tr>
			<td>
				Монетный двор
			</td>
			<td>
				<?= $mints[$coin->mint_id] ?>
			</td>
		</tr>
		<tr>
			<td>
				Аверс
			</td>
			<td>
				<?= $coin->obverse_desc ?>
			</td>
		</tr>
		<tr>
			<td>
				Реверс
			</td>
			<td>
				<?= $coin->reverse_desc ?>
			</td>
		</tr>
		<tr>
			<td>
				Тираж
			</td>
			<td>
				<?= $coin->mintage ?>
			</td>
		</tr>
		<tr>
			<td>
				Качество чеканки
			</td>
			<td>
				<?= $qualities[$coin->quality_id] ?>
			</td>
		</tr>
		<tr>
			<td>
				Металл
			</td>
			<td>
				<?= $materials[$coin->material_id] ?>
			</td>
		</tr>
		<tr>
			<td>
				Дата чеканки
			</td>
			<td>
				<?= $coin->release ?>
			</td>
		</tr>
		<? if ($coin->type_id == 2) { ?>
		<tr>
			<td>
				Серия
			</td>
			<td>
				<?= $series[$coin->serios_id] ?>
			</td>
		</tr>
		<tr>
			<td>
				Проба
			</td>
			<td>
				<?= $coin->sample ?>
			</td>
		</tr>
		<? } ?>
	</table>
</div>
<div class="clearfix"></div>
<div class="col-md-8">
	<h3>Физические свойства</h3>
	<table class="table table-striped table-bordered">
		<tr>
			<td>
				Диаметр
			</td>
			<td>
				<?= $coin->diameter ?>
			</td>
		</tr>
		<tr>
			<td>
				Вес
			</td>
			<td>
				<?= $coin->weight ?>
			</td>
		</tr>
		<tr>
			<td>
				Толщина
			</td>
			<td>
				<?= $coin->thickness ?>
			</td>
		</tr>
	</table>
</div>
<div class="clearfix"></div>
<? include('tmpl/footer.php'); ?>