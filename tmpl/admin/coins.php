<? include('tmpl/admin/header.php'); ?>
<ol class="breadcrumb">
	  <li><a href="/admin/coins.php">Монеты</a></li>
	  <? if ($period->id != 0) { ?>
	  <li class="active"><?= $period->name ?></li>
	  <? } ?>
</ol>

<?= $message ?>
<h1>Монеты периода <?= ($period->name) ?> 
<? if ($period->id != 0) { ?><a href="/admin/coins.php?period_id=<?=$period->id?>&task=new" class="btn btn-success">Добавить</a><? } ?></h1>
<? if ($coins) { ?>
<table class="table table-striped table-bordered">
	<tr>
		<th>
			Наименование
		</th>
		<th>
			Тип
		</th>
		<th>
			Монетный двор
		</th>
		<th>
			Действия
		</th>
	</tr>
	<? foreach ($coins as $key => $coin) { ?>
	<tr>
		<td>
			<a href="/admin/coins.php?coin_id=<?= $coin->id ?>"><?= $coin->name ?></a>
		</td>
		<td>
			<?= $types[$coin->type_id] ?>
		</td>
		<td>
			<?= $mints[$coin->mint_id] ?>
		</td>
		<td>
			<a href="/admin/coins.php?coin_id=<?=$coin->id?>&task=edit">редактировать</a>
			<a href="/admin/coins.php?coin_id=<?=$coin->id?>&task=delete">удалить</a>
		</td>
	</tr>
	<? } ?>
</table>
<? } else { ?>
	<div role="alert" class="alert alert-info">
      <strong>Ничего не найдено</strong> К сожалению мы не нашли монеты отвечающие вашему запросу
    </div>
<? } ?>
<? include('tmpl/footer.php'); ?>