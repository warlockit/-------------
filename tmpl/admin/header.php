<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
	<title>Монеты России</title>
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.2/css/bootstrap.min.css">
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.2/css/bootstrap-theme.min.css">
</head>
<body>
	<div class="container">
		<img src="/images/logo.png" alt="Coins.ru">
	</div>
	<div class="container">
		<div class="col-md-4">
			<? if (isset($menu)) { ?>
				<? foreach ($menu as $key => $stage) { ?>
					<div class="panel panel-default">
						<div class="panel-heading">
								<?= $stage->name ?>
							</div>
							<div class="panel-body">
							<? if (count($stage->periods) > 0) { ?>
							<ul class="nav nav-pills">
								<? foreach($stage->periods as $p) { ?>
									<li<?= ($period->id == $p->id ? ' class="active"' : '') ?>>
										<a href="/admin/coins.php?period_id=<?= $p->id ?>">
										<?= $p->name ?></a>
									</li>
								<? } ?>
							</ul>
							<? } ?>
						</div>
					</div>
				<? } ?>
			<? } ?>	
		</div>
		<div class="col-md-8">