<? include('tmpl/admin/header.php'); ?>
<?= $message ?>
<h1><?= ($coin->id ? 'Редактирование монеты' : 'Создание новой монеты')?></h1>
<form action="" method="POST">
	<div class="form-group">
		<label for="period_id">Период</label>
		<select name="period_id" id="period_id" class="form-control">
			<option value="0">Не выбрано</option>
			<? foreach ($stages as $key => $stage) { ?>
				<optgroup label="<?= $stage->name ?>">
				<? foreach ($stage->periods as $key => $p) { ?>
					<option value="<?= $p->id ?>" <?= ($period->id == $p->id ? 'selected' : '') ?>><?= $p->name ?></option>
				<? } ?>
				</optgroup>
			<? } ?>
		</select>
	</div>
	<div class="form-group">
		<label for="type_id">Тип</label>
		<select name="type_id" id="type_id" class="form-control">
			<? foreach ($types as $key => $value) { ?>
				<option value="<?= $key ?>" <?= ($coin->type_id == $key ? 'selected' : '') ?>><?= $value ?></option>
			<? } ?>
		</select>
	</div>
	<div class="form-group">
		<label for="nominal_id">Номинал</label>
		<select name="nominal_id" id="nominal_id" class="form-control">
			<option value="0">Не выбрано</option>
			<? foreach ($nominals as $key => $value) { ?>
				<option value="<?= $key ?>" <?= ($coin->nominal_id == $key ? 'selected' : '') ?>><?= $value ?></option>
			<? } ?>
		</select>
	</div>
	<div class="form-group">
		<label for="year">Год</label>
		<input type="text" value="<?= $coin->year ?>" name="year" id="year" class="form-control">
	</div>
	<div class="form-group">
		<label for="name">Название</label>
		<input type="text" value="<?= $coin->name ?>" name="name" id="name" class="form-control">
	</div>
	<div class="form-group">
		<label for="serios_id">Серия</label>
		<select name="serios_id" id="serios_id" class="form-control">
			<option value="0">Не выбрано</option>
			<? foreach ($series as $key => $value) { ?>
				<option value="<?= $key ?>" <?= ($coin->serios_id == $key ? 'selected' : '') ?>><?= $value ?></option>
			<? } ?>
		</select>
	</div>
	<div class="form-group">
		<label for="mint_id">Монетный двор</label>
		<select name="mint_id" id="mint_id" class="form-control">
			<option value="0">Не выбрано</option>
			<? foreach ($mints as $key => $value) { ?>
				<option value="<?= $key ?>" <?= ($coin->mint_id == $key ? 'selected' : '') ?>><?= $value ?></option>
			<? } ?>
		</select>
	</div>
	<div class="form-group">
		<label for="mint_id">Аверс</label>
		<textarea name="obverse_desc" id="obverse_desc" class="form-control" cols="30" rows="10"><?= $coin->obverse_desc ?></textarea>
	</div>
	<div class="form-group">
		<label for="mint_id">Реверс</label>
		<textarea name="reverse_desc" id="reverse_desc" class="form-control" cols="30" rows="10"><?= $coin->reverse_desc ?></textarea>
	</div>
	<div class="form-group">
		<label for="mintage">Тираж</label>
		<input type="text" value="<?= $coin->mintage ?>" name="mintage" id="mintage" class="form-control">
	</div>
	<div class="form-group">
		<label for="diameter">Диаметр</label>
		<input type="text" value="<?= $coin->diameter ?>" name="diameter" id="diameter" class="form-control">
	</div>
	<div class="form-group">
		<label for="weight">Вес</label>
		<input type="text" value="<?= $coin->weight ?>" name="weight" id="weight" class="form-control">
	</div>
	<div class="form-group">
		<label for="thickness">Толщина</label>
		<input type="text" value="<?= $coin->thickness ?>" name="thickness" id="thickness" class="form-control">
	</div>
	<div class="form-group">
		<label for="sample">Проба</label>
		<input type="text" value="<?= $coin->sample ?>" name="sample" id="sample" class="form-control">
	</div>
	<div class="form-group">
		<label for="release">Дата создания</label>
		<input type="text" value="<?= $coin->release ?>" name="release" id="release" class="form-control">
	</div>
	<div class="form-group">
		<label for="quality_id">Качество</label>
		<select name="quality_id" id="quality_id" class="form-control">
			<option value="0">Не выбрано</option>
			<? foreach ($qualities as $key => $value) { ?>
				<option value="<?= $key ?>" <?= ($coin->quality_id == $key ? 'selected' : '') ?>><?= $value ?></option>
			<? } ?>
		</select>
	</div>
	<div class="form-group">
		<label for="material_id">Материал</label>
		<select name="material_id" id="material_id" class="form-control">
			<option value="0">Не выбрано</option>
			<? foreach ($materials as $key => $value) { ?>
				<option value="<?= $key ?>" <?= ($coin->material_id == $key ? 'selected' : '') ?>><?= $value ?></option>
			<? } ?>
		</select>
	</div>
	<?= ($coin->id ? '<input type="hidden" name="id" value="'.$coin->id.'">' : '') ?>
	<div class="form-group text-right">
		<input type="submit" class="btn btn-success" value="Сохранить">
	</div>
</form>
<? include('tmpl/footer.php'); ?>