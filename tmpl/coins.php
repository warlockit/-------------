<? include('tmpl/header.php'); ?>
<ol class="breadcrumb">
	  <li><a href="/coins.php">Монеты</a></li>
	  <? if ($period->id != 0) { ?>
	  <li class="active"><?= $period->name ?></li>
	  <? } ?>
</ol>

<?= $message ?>
<h1>Монеты периода <?= ($period->name) ?></h1>
<div class="well">
	<form action="" method="GET" class="form-horizontal">
		<div class="form-group">
			<label for="nominal_id" class="col-md-3 control-label">Номинал</label>
			<div class="col-md-3">
				<select name="nominal_id" id="nominal_id" class="form-control">
					<option value="">Не выбрано</option>
					<? foreach ($nominals as $key => $value) { ?>
						<option value="<?= $key ?>" <?= (isset($params['nominal_id']) && $params['nominal_id'] == $key ? 'selected' : '') ?>><?= $value ?></option>
					<? } ?>
				</select>
			</div>
			<label for="year_id" class="col-md-1 control-label">Год</label>
			<div class="col-md-2">
				<select name="year" id="year" class="form-control">
					<option value="">Не выбрано</option>
					<? foreach ($years as $key => $value) { ?>
						<option value="<?= $value ?>" <?= (isset($params['year']) && $params['year'] == $value ? 'selected' : '') ?>><?= $value ?></option>
					<? } ?>
				</select>
			</div>
		</div>
		<div class="form-group">
			<label for="mint_id" class="col-md-3 control-label">Монетный двор</label>
			<div class="col-md-6">
				<select name="mint_id" id="mint_id" class="form-control">
					<option value="">Не выбрано</option>
					<? foreach ($mints as $key => $value) { ?>
						<option value="<?= $key ?>" <?= (isset($params['mint_id']) && $params['mint_id'] == $key ? 'selected' : '') ?>><?= $value ?></option>
					<? } ?>
				</select>
			</div>
			<div class="col-md-2">
				<input type="submit" value="Применить" class="btn btn-primary">
			</div>
		</div>
		<input type="hidden" name="period_id" value="<?= $params['period_id']; ?>">
		<div class="clearfix"></div>
	</form>
</div>
<? if ($coins) { ?>
<table class="table table-striped table-bordered">
	<tr>
		<th>
			Наименование
		</th>
		<th>
			Тип
		</th>
		<th>
			Монетный двор
		</th>
	</tr>
	<? foreach ($coins as $key => $coin) { ?>
	<tr>
		<td>
			<a href="/coins.php?coin_id=<?= $coin->id ?>"><?= $coin->name ?></a>
		</td>
		<td>
			<?= $types[$coin->type_id] ?>
		</td>
		<td>
			<?= $mints[$coin->mint_id] ?>
		</td>
	</tr>
	<? } ?>
</table>
<? } else { ?>
	<div role="alert" class="alert alert-info">
      <strong>Ничего не найдено</strong> К сожалению мы не нашли монеты отвечающие вашему запросу
    </div>
<? } ?>
<? include('tmpl/footer.php'); ?>